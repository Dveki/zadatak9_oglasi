<?php

require("inc/config.php");
    require("inc/db_config.php");
    require("inc/functions.php");

?>
<!DOCTYPE html>
<html>
    <head>

    </head>
    <body>
                <form action="realinsert.php" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <legend>Insert New Ad</legend>

                        <label for="newadname">Name</label><br>
                        <input type="text" name="newadname" required><br><br>

                        <label>Category</label><br>
                        <select type="text" name="category"  value="" required="required">
                        <br>
                        <option value="">- Choose -</option>";
                        <br>
                        <?php
                        $sql = "SELECT * FROM category";
                        $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

                        if(mysqli_num_rows($result)>0)
                        {

                        while ($record=mysqli_fetch_array($result,MYSQLI_BOTH))
            
                         echo "<option value=\"$record[id]\">$record[namecategory]</option>";
                         }
                        ?>
                        </select><br><br>
                        
                        <label for="description">Description</label><br>
                        <textarea type="text" name="description" required rows="5" cols="50"></textarea><br>

                        <label for="file">Image</label><br>
                        <input type="file" name="file" required><br><br>

                        <label for="price">Price (RSD)</label><br>
                        <input type="number" name="price" required><br><br>

                        <input type="submit" name="submit" value="Insert"><br>
                        <a href="index.php">Return to Homepage</a>

                    </fieldset>
                </form>
    </body>
</html>



<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="inc/style.css" />
<title>Oglas</title>
</head>
<body>
<div class="wrapper">
<?php
	require("inc/header.php");
    require("inc/aside.php");
    require("inc/products.php");    
    require("inc/footer.php");    
    
?>
</div>
</body>
</html>
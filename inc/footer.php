        <footer class="footer">
            <!--START CONTACT INFO-->
            <p class="footer__p">Posted by: Damir</p>
            <p class="footer__p">Contact information: 
                <a href="mailto:someone@example.com">someone@example.com</a>
            </p>
            <p class="footer__p">&copy; Copyright</p>
            <!--END CONTACT INFO-->
        </footer>

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 18, 2018 at 03:19 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ads`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `namecategory` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `namecategory`) VALUES
(1, 'Nekretnine'),
(2, 'Tehnika'),
(3, 'Bicikli'),
(4, 'Automobili');

-- --------------------------------------------------------

--
-- Table structure for table `productsads`
--

DROP TABLE IF EXISTS `productsads`;
CREATE TABLE IF NOT EXISTS `productsads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(80) NOT NULL,
  `price` float NOT NULL,
  `date` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productsads`
--

INSERT INTO `productsads` (`id`, `id_category`, `name`, `description`, `image`, `price`, `date`) VALUES
(1, 1, 'kuca 2', 'Prodajemo lepu, atraktivnu kucu, gasno grejanje i kaljeve peci, podovi parket, dve terase, dupla garaza, veci betonsko armirani bazen, podrum, legalizovano, konverzija zemljista uradjena.', 'inc/images/house', 500000, '2018-04-16 22:00:00'),
(2, 2, 'Sony fotoaparat', 'Star godinu i po, očuvan, ispravan. Na poklon i tronožac. --- 10 megapixels | 3″ screen | 33 – 105 mm (3.2×) Max resolution 3456 x 2592 Battery description - 2 x AA Alkaline batteries Weight (inc. batteries) - 167 g (0.37 lb / 5.89 oz) Dimensions - 98 x 61 x 27 mm (3.86 x 2.4 x 1.06″)', 'inc/images/sony', 3000, '2018-04-16 16:00:00'),
(3, 2, 'Samsung 65MU6102 ', 'Opis proizvoda     65MU6172\r\nMaksimalna rezolucija 3840x2160\r\nSmart \r\nKontrast Mega\r\nHDMI 3, USB 2\r\nTehnologija LED\r\nIntegrirani WiFi Da\r\nDVB-TCS2(T2 Ready)', 'inc/images/samsungtv', 65000, '2018-04-15 13:00:00'),
(4, 1, 'kuca 1', 'Spratna kuca sa lokalom velikom garazom i dosta pratecih objekata Mogucnost stanovanja dve porodice grejanje : gas, centralno, ugalj, drva,...', 'inc/images/house2.jpg', 658220, '2018-04-18 12:27:24'),
(5, 3, 'Capriolo Anaconda 22', 'Capriolo Anaconda 22, predji amortizer, prednji disk, uz merač brzine, na jednom mestu samo je ogreban kao sto se vidi na slikama, bicikl skoro nije vozen. Za vise informacija kontakt', 'inc/images/anaconda', 15000, '2018-04-17 10:00:00'),
(6, 4, '2006 Alfa Romeo 159 159 JTDM', 'Presao 240.000 km, dizel, 1900 cm3, Manuaelni menjac.\r\nDomace tablice, nije registrovan', 'inc/images/alfa', 580000, '2018-04-17 20:00:00'),
(7, 3, 'Trek 8900 ', 'Carbon ram , shimano deore XT, alu točkovi, nove gume, malo vozen, grip shift, ram 54 velicina', 'inc/images/trek.jpg', 30000, '2018-04-18 14:28:11'),
(8, 3, 'Crveni bicikl', 'Bicikl u solidnom stanju, nove unutrasnje gume, nove kocnice i novi blatobrani. Spoljasnje gume ok. Bez ulaganja. Menjaci nastelovani. Sedi i vozi', 'inc/images/crvenibike.jpg', 10000, '2018-04-18 14:31:53');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL,
  `privilege` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `active` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `active`, `role_id`) VALUES
(1, 'svetli', '6a0762d51c3a98ac26fbe5a4a123106e', 'Jovan', 'ON admin', 1, 1),
(2, 'mrki', '6a0762d51c3a98ac26fbe5a4a123106e', 'Marko', 'ON korisnik', 1, 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
